#======================================[ Flexbox ]


[Link's]

	.<https://cssreference.io/flexbox/>
	.<https://www.w3schools.com/css/css3_flexbox.asp>
	.<https://www.origamid.com/curso/css-flexbox/1-1-css-flexbox>
	.<https://triangulo.dev/posts/guia-completo-flexbox>

. O CSS Flexible Box Layout, conhecido como Flexbox, é um MODELO DE LAYOUT da Web CSS3. Está no estágio de recomendação de candidatos (CR) do W3C. O layout flexível permite que os elementos responsivos dentro de um contêiner sejam organizados automaticamente, dependendo do tamanho da tela (ou dispositivo)



	- A especificação descreve um modelo de caixa CSS otimizado para design de interface de usuário.


	- No modelo de layout flexível, os filhos de um CONTÊINER FLEXÍVEL podem ser dispostos em qualquer direção e podem "flexionar" seus tamanhos, tanto AUMENTANDO para preencher o espaço não utilizado quanto ENCOLHENDO para evitar o transbordamento do pai(Bloco Principal).



**[Antes do módulo Flexbox Layout, havia quatro modos de layout]**

	[ CSS 2.1 definiu quatro modos de layout ]


	. block layout: projetado para layout de documentos

	. inline layout: projetado para dispor o texto

	. table layout: projetado para dispor dados 2D em um formato tabular

	. Posicionado: para a posição explícita de um elemento


[ Visão geral ]

	. O layout flexível é superficialmente semelhante ao layout de blocos. 

	. O Módulo de Layout de Caixa Flexível torna mais fácil projetar uma estrutura de layout responsiva e flexível sem usar flutuação ou posicionamento.

	. ele ganha ferramentas simples e poderosas para DISTRIBUIR ESPAÇO e ALINHAR CONTEÚDO de maneiras que os aplicativos da web e páginas da web complexas geralmente precisam. O conteúdo de um flex container:



		- pode ser disposto em qualquer direção de fluxo (para a esquerda, para a direita, para baixo ou mesmo 	para cima!)

		- podem ter sua ordem de exibição invertida ou reorganizada na camada de estilo 

		- podem “flexionar” seus tamanhos para atender ao espaço disponível


		- podem ser alinhados em relação ao seu contêiner ou um ao outro no secundário ( cruz )

		- pode ser recolhido dinamicamente ou não recolhido ao longo do eixo principal , preservando o tamanho cruzado do contêiner


#---------------[ display


.  flex container torna-se flexível definindo a propriedade display(Exibição) para flex:

Flex : Colocar cada bloco um do lado do outro



	. Flex layout so items tem altura igual 


==============================================[ Propriedades ]



As propriedades do flex container são:


#---------------------[flex-direction - <https://cssreference.io/property/flex-direction/>


. A flex-direction propriedade define EM QUAL DIREÇÃO o contêiner deseja EMPILHAR os itens flexíveis.

[row]

	> Os itens do flexbox são ordenados da MESMA MANEIRA que a direção do texto , 
	ao longo do EIXO PRINCIPAL.

	> Os itens ficam em Linha


[row-reverse]

	> Os itens do flexbox são ordenados de MANEIRA OPOSTA à direção do texto , 
	ao longo do  EIXO PRINCIPAL.

		> Os itens ficam em Linha REVERSA, ou seja 3,2,1

[column]

	> Os itens do flexbox são ordenados da MESMA MANEIRA que a direção do texto , 
		ao longo do EIXO CRUZADO .

		> Os itens ficam em uma única coluna, um embaixo do outro

[column-reverse]


	> Os itens do flexbox são ordenados da MANEIRA OPOSTA à direção do texto , 
	ao longo do EIXO CRUZADO .


		>  Os itens ficam em uma única coluna, um embaixo do outro, porém em ordem reversa: 3,2,1

#---------------------[flex-wrap - <https://cssreference.io/property/flex-wrap/>

. Por padrão, os elementos-filhos de um contêiner flex serão dispostos ao longo dos eixos, SEM QUEBRAREM EM NOVAS LINHAS e COLUNAS. Para controlar este comportamento, podemos utilizar a propriedade flex-wrap.

	- O valor padrão para esta propriedade é nowrap, que define que os elementos serão dispostos em uma única linha.

. Define se os itens do flexbox APARECEM em uma ÚNICA LINHA ou em VÁRIAS LINHAS em um contêiner do flexbox.

. Definem se os itens devem QUEBRAR ou NÃO A LINHA.


[wrap]

	> Os itens do flexbox serão distribuídos entre várias linhas, se necessário.

	> QUEBRA A LINHA assim que um dos itens flex não puder mais ser compactado.

[nowrap]

	> Os itens do flexbox PERMANECERÃO EM UMA ÚNICA LINHA, não importa o que aconteça , e eventualmente transbordarão, se necessário.

	> VALOR PADRÃO. Não permite a quebra de linha.

[wrap-reverse]


	> Os itens do flexbox serão distribuídos entre várias linhas, se necessário. Qualquer linha adicional aparecerá antes da anterior.

	> QUEBRA A LINHA assim que um dos itens flex não puder mais ser compactado. A quebra é na direção contrária, ou seja para a linha acima



#---------------------[flex-flow


. é uma propriedade abreviada para DEFINIR PROPRIEDADES flex-direction e flex-wrap.

. A propriedade Flex-Flow é uma atalho para as propriedades Flex-Direction e Flex-Wrap. 


	flex-flow: direction -> row, row-reverse, column e column reverse | wrap -> nowrap , wrap e wrap-reverse




#---------------------[justify content(justificar o conteúdo)

. Define como os itens flexbox / grade SÃO ALINHADOS de acordo com o eixo principal , dentro de um contêiner flexbox / grade.

. propriedade é usada para alinhar os itens flexíveis:

. ALINHAR os itens flex no container DE ACORDO COM A DIREÇÃO.

	-A  propriedade só funciona SE OS ITEnS ATUAIS NÃO OCUPAM TODO O CONTAINER.


	.Excelente propriedade para ser usada em casos que você deseja ALINHAR um item na PONTA ESQUERDA 
	e outro na DIREITA em um simples header com marca e navegação.


[flex-start]

	> Os itens flexbox / grade são empurrados em DIREÇÃO AO INÍCIO do eixo principal do contêiner.

[flex-end]

	> Os itens flexbox / grade são empurrados em DIREÇÃO AO FINAL do eixo principal do contêiner.


[center]

	> Os itens flexbox / grade SÃO CENTRALIZADOS ao longo do eixo principal do contêiner.

[space-between]

	> O espaço restante é DISTRIBUIDO ENTRE OS ITENS flexbox / grade. 

[space-around]

	> O espaço restante é DISTRIBUIDO EM TORNO dos itens flexbox / grade: isso adiciona espaço antes do primeiro item e depois do último.


#---------------------[align-items

. Define como OS ITENS DO FLEXBOX SÃO ALINHADOS DE ACORDO COM O EIXO CRUZADO(Vertical), dentro de uma linha de um contêiner do flexbox.

	- é usada para alinhar os itens flexíveis.

	- A linha os itens flex de acordo com o eixo do container.

	-  Essa propriedade permite o tão  ALINHAMENTO NO EIXO VERTICAL.

[stretch]

	>Os itens do flexbox SE ESTENDERÃO POR TODO O EIXO TRANSVERSAL.

	Por padrão, o eixo cruzado é vertical. Isso significa que os itens do flexbox preencherão todo o espaço vertical.

[flex-start;]

	>Os itens do flexbox são alinhados no INICÍO do eixo cruzado .

	Por padrão, o eixo cruzado é vertical(|). Isso significa que os itens do flexbox serão alinhados verticalmente na PARTE SUPERIOR.

	-Alinha os itens ao Início

[flex-end]

	>Os itens do flexbox são ALINHADOS NO FINAL do eixo cruzado .

	Por padrão, o eixo cruzado é vertical. Isso significa que os itens do flexbox serão alinhados verticalmente na PARTE INFERIOR .

[center]

	>Os itens do flexbox são ALINHADOS NO CENTRO do eixo cruzado .

	Por padrão, o eixo cruzado é vertical. Isso significa que os itens do flexbox serão 
	CENTRALIZADOS VERTICALMENTE.

[baseline]

	>Os itens do flexbox são ALINHADOS NA LINHA DE BASE do eixo cruzado .

	Por padrão, o eixo cruzado é vertical. Isso significa que os itens do flexbox
	 se alinharão para que a linha de base de seu texto seja alinhada ao longo de uma linha horizontal.




#---------------------[ align-content


> DEFINE COMO CADA LINHA É ALINHADA em um contêiner flexbox / grade. Só se aplica se flex-wrap: wrap ESTIVER PRESENTE e se houver várias linhas de itens flexbox / grade.

	. A align-content propriedade modifica o comportamento da propriedade flex-wrap . É semelhante a itens de alinhamento, mas em vez de alinhar itens flexíveis, ELE ALINHA AS LINHAS FLEXÍVEIS.

	Nota: DEVE haver várias linhas de itens para esta propriedade ter algum efeito!

	DICA: Use a propriedade justify-content para alinhar os itens no eixo principal (horizontalmente).


[flex-start]

	> Cada linha preencherá APENAS O ESPAÇO QUE NECESSITA. Todos eles se moverão em direção ao INÍCIO DO EIXO TRANSVERSAL do contêiner flexbox / grade.

		-ALINHA	 TODAS AS LINHAS DE ITENS AO início.

[flex-end]

	> Cada linha preencherá APENAS O ESPAÇO DE QUE NECESSITA. Todos eles se moverão em direção ao FINAL DO EIXO TRANSVERSAL do contêiner flexbox / grade.

		-ALINHA TODAS AS LINHAS DE ITENS AO FINAL.


[center]

	> Cada linha preencherá apenas O ESPAÇO DE QUE NECESSITA. Todos eles se moverão em direção ao CENTRO DO EIXO TRANSVERSAL do contêiner flexbox / grade.

		-ALINHA TODAS AS LINHAS DE ITENS AO CENTRO.

[space-between]


	> Cada linha preencherá apenas O ESPAÇO DE QUE NECESSITA. 

		*O ESPAÇO RESTANTE APARECERÁ ENTRE AS LINHAS .

	. Cria um espaço IGUAL ENTRE AS LINHAS. Mantendo a Primeira
	Agrupada no TOPO  e a ÚLTIMA no bottom.


[space-around]

	> Cada linha preencherá apenas o espaço de que necessita . O espaço restante  será DISTRIBUIDO IGUALMENTE EM TORNO DAS LINHAS: antes da primeira linha, entre as duas e depois da última.

	. Cria uma espaçamento ENTRE AS LINHAS. Os espaçamentos do meio
	são DUAS VEZES MAIORES que o TOP e BOTTOM




#------------------------[ align-self ] -> ÚNICO ELEMENTO


. Funciona como align-items, MAS SE APLICA APENAS A UM ÚNICO ITEM do flexbox, em vez de todos eles.





[auto]

	> O destino usará o valor de align-items.


[flex-start]

	> Se o contêiner align-items: center e o alvo tiverem align-self: flex-start, apenas o alvo estará NO INÍCIO DO EIXO CRUZADO.

	Por padrão, isso significa que ele será ALINHADO VERTICALMENTE NA PARTE SUPERIOR  .


[flex-end]

	> Se o contêiner align-items: center e o alvo estiverem align-self: flex-end, apenas o alvo estará NO FINAL DO EIXO CRUZADO.

	.Por padrão, isso significa que ele será ALINHADO VERTICALMENTE NA PARTE INFERIOR.


[center]

	> Se o contêiner align-items: flex-start e o alvo estiverem align-self: center, apenas o alvo estará NO CENTRO DO EIXO CRUZADO.

	Por padrão, isso significa que ele será CETRALIZADO VERTICALEMNTE.

[baseline]

	> Se o contêiner tiver align-items: center e o alvo tiver align-self: baseline, apenas o alvo estará NA LINHA BASE DO EIXO CRUZADO.

	Por padrão, isso significa que ele será ALINHADO AO LONGO DA LINHA DE BASE DO TEXTO.


[stretch]


	> Se o contêiner align-items: center e o alvo tiverem align-self: stretch, apenas o alvo SE ESTENDERÁ AO LONGO DE TODO O EIXO TRANSVERSAL.






#------------------------[ flex-grow ] -> ÚNICO ELEMENTO


. A propriedade flex-grow recebe como valor UM NÚMERO que represente o 

	- "fator de crescimento". 

Esse número define como o elemento-filho REAGIRÁ A SOBRA DE ESPAÇO EM UMA CONTÊINER FLEX. 
	
	- A sobra de espaço é calculada pelo o tamanho total do contêiner flex menos o espaço base total ocupado pelos elementos-filhos. Por padrão, o valor de flex-grow é 0.


. Define quanto um item flexbox DEVE CRESCER se houver espaço disponível.

	
	> especifica quanto o item crescerá em relação ao resto dos itens flexíveis dentro do mesmo contêiner.


[ 0 ]

	> O elemento NÃO CRESCERÁ se houver espaço disponível. Ele só usará o ESPAÇO DE QUE PRECISA.

[ 1 ]
	
	> O elemento CRESCERÁ  por um fator de 1 . Ele preencherá o ESPAÇO RESTANTE se nenhum outro item do flexbox tiver um flex-grow valor.


[ 2 ]

	>Como o valor flex-grow é relativo , seu comportamento depende do valor dos irmãos do item flexbox .




 
#------------------------[  flex-shrink ] - <https://developer.mozilla.org/en-US/docs/Web/CSS/flex-shrink>


. Quando FALTAR ESPAÇO em um contêiner flex, os seus elementos-filhos são encolhidos de acordo com o fator de encolhimento. Por padrão, o valor de flex-shrink é 1.

. especifica como o item será REDUZIDO em relação ao restante dos itens flexíveis dentro do mesmo contêiner.





	. Define quanto um item flexbox deve encolher SE NÃO HOUVER ESPAÇO SUFICIENTE DISPONÍVEL.



[ 0 ]

	> O ELEMENTO NÃO ENCOLHERÁ: ele manterá a largura necessária e não envolverá seu conteúdo. Seus irmãos encolherão para dar espaço ao elemento de destino.

	Porque o elemento de destino não vai quebrar o seu conteúdo, há uma chance do conteúdo do recipiente flexbox para estouro .	

[ 1 ]

	> Se não houver espaço suficiente disponível no eixo principal do contêiner, O ELEMENTO ENCOLHERÁ por um fator de redução da flexibilidade de 1 e envolverá seu conteúdo.


[ 2 ]

	> Como o valor do flex-shrink é relativo , seu comportamento depende do valor dos irmãos do item flexbox .

		. Se o tamanho de todos os itens flexíveis for MAIOR do que o contêiner flexível, os itens ENCOLHERÃO Para caber de acordo com



#------------------------[ flex-basis


	. Define o tamanho inicial de um item do flexbox.

		- A propriedade flex-basis especifica o comprimento inicial de um item flexível.

[ auto ]

	>O elemento será dimensionado automaticamente com base em seu conteúdo, ou em qualquer valor heightou widthse eles forem definidos.


[ 80px ]

Você pode definir valores de pixel ou (r) em . O elemento envolverá seu conteúdo para evitar qualquer estouro.





#---------------------[ Order


. Define A ORDEM DE UM ITEM do flexbox.

	
	>A propriedade order ESPECIFICA A ORDEM de um item flexível em relação ao restante dos itens flexíveis dentro do mesmo contêiner.


			:: order: number -> O number represente o número do item flexivel, a qual deseja manipular.

[ 0 ]

	>A ordem dos itens flexbox é aquela DEFINIDA no código HTML.




[  -1 ];

Você pode usar valores negativos .




#------------------------[[ FLEX ]]




A flex propriedade é uma propriedade abreviada para:

	. flex-grow
	. flex-shrink
	. FLEX-BASIS

A propriedade flex define o comprimento flexível em itens flexíveis.




	flex: flex-grow flex-shrink flex-basis|auto|initial|inherit;


	> flex-grow Um número que especifica quanto o item irá crescer em relação ao resto dos itens flexíveis

	> flex-shrink Um número que especifica o quanto o item vai encolher em relação ao resto dos itens flexíveis

	> flex-basis O comprimento do item. Valores legais: "auto", "inherit" ou um número seguido por "%", "px", "em" ou qualquer outra unidade de comprimento




		. auto: O mesmo que - 1 1 auto.

		. initial: Igual a  0 1 auto. Read about initial
		
		. none: Igual  0 0 auto.


		. inherit:	Herda essa propriedade de seu elemento pai. 



#---------------------[ flex-flow - <https://www.w3.org/TR/css-flexbox-1/#flex-flow-property>


. é uma abreviação para definir as propriedades flex-direction e flex-wrap , que juntas definem os: 

	. eixos principal e 
	. transversal do contêiner flexível.


.A propriedade flex-direction especifica COMO OS flex items SÃO COLOCADOS no flex container, definindo a direção do eixo principal do flex container . Isso determina a direção em que os flex items são dispostos.



	[ROW]

		-  Valor inicial. O eixo principal está em linha, sem agrupamento.
   		(Os itens encolherão para caber ou transbordarão.)


	[WRAP]

		-  O eixo principal é a direção do bloco (de cima para baixo)
   e as linhas são ajustadas na direção inline (para a direita). 


   	[ row-reverse wrap-reverse]

   		- O eixo principal é o oposto da direção em linha
   (direita para esquerda). Novas linhas são ajustadas para cima.


   **[Exemplos]**

   		<https://www.w3schools.com/cssref/playit.asp?filename=playcss_flex-flow>







[=============================================================================]



#=================================[ Grid Layout ]


.o layout de grade CSS ou grade CSS CRIA LAYOUTS DE WEB design responsivos complexos com mais FACILIDADE e CONSISTÊNCIA entre os navegadores.

	[MOTIVAÇÃO PARA O GRID LAYOUT]

		> A grade CSS pode CRIAR LAYOUTS mais assimétricos do que a grade anterior e as opções de layout, como os floats CSS .

		> Ele também permite um CÓDIGO MAIS PADRONIZADO que funciona em todos os navegadores. 

		[FALHA DO CSS FLEXBOX]

			> Embora o CSS flex box suporte layouts flexíveis e forneça a flexibilidade de criar layouts complexos, ele falha quando surge a necessidade de criar layouts responsivos em espaço bidimensional.



	[a unidade fr ]

		>A unidade "fr" é freqüentemente usada com layout de grade CSS.  A unidade "fr", parte da especificação do layout da grade CSS, representa uma FRAÇÃO DO ESPAÇO RESTANTE no contêiner da grade.

		*2fr -> ocupa o dobro de espaço/tamanho de um contêiner / triplo de espaço ...


. CSS Grid Layout é uma nova especificação do CSS e ela veio para RESOLVER PRATICAMENTE TODOS OS PROBLEMAS DE POSICIONAMENTO na tela que existem.


.CSS Grid Layout é uma PODEROSA ESPECIFICAÇÃO que se for combinada com outras partes do CSS, tal como flexbox, possibilita a criação de layouts que até então eram impossíveis de serem criados com CSS. Tudo começa com a criação de um grid dentro de um grid container.


	[O que é grid?]




		> Grid(rede/grade) é uma malha formada pela interseção de:

			- um CONJUNTO DE LINHAS HORIZONTAIS(----) e 
			- um CONJUNTO DE LINHAS VERTICAIS(|) 

		– um dos conjuntos define colunas e outro linhas. 

		Dentro de um  grid, respeitando-se a configuração criada pelas suas linhas, pode-se inserir elementos da marcação. As CSS grid layout preveem as seguintes funcionalidades:

			[Dimensões fixas ou flexíveis]

			>Você pode criar grids com dimensões fixas – por exemplo: 

				. definindo dimensões em PIXELS(px). Ou 
				. criar grids com dimensões flexíveis definindo-as com uso de 

					- PORCENTAGEM ou da 
					- NOVA UNIDADE CSS fr criada para esse propósito.




			[Posicionamento de itens]


			>Você pode POSICIONAR COM PRECISÃO itens de uma página usando: 

				- O NÚMERO QUE DEFINE UMA LINHA do grid, 
				- USANDO NOMES ou ainda 
				- FAZENDO REFERÊNCIAS região do grid.



			[Criação de grids adicionais]
				
			>Além da possibilidade de se criar um GRID INICIAL para o layout a Especificação prevê também a possibilidade de se ADICIONAR LINHAS e COLUNAS  para layoutar conteúdos ADICIONADOS FORA DO GRID INICIAL. 


				.Funcionalidades tal como adicionar "tantas colunas quanto necessárias em um grid container existente" são previstas nas Especificações.





			[ Alinhamento ]

			>Estão previstas funcionalidades de alinhamento que possibilitam 

				- CONTROLAR O ALINHAMENTO DOS ITENS de uma página posicionados no grid 
				- e também o ALINHAMENTO DO PRÓPRIO GRID COMO UM TODO.


			[Controle sobre conteúdos sobrepostos]

			>Em uma célula do grid podem ser POSICIONADOS MAIS DE UM ITEM DA PÁGINA e também é possível que se DEFINA SOBREPOSIÇÃO PARCIAL DE ÁREAS. Esse controle de layers é feito com uso de z-index (en-US).


	.<https://en.wikipedia.org/wiki/CSS_grid_layout>
	.<https://developer.mozilla.org/pt-BR/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout>



[Sigla de unidade fracionária]


.A unidade Fr: Fr é uma unidade fracionária.


	>A unidade Fr é uma entrada que CALCULA AUTOMATICAMENTE as divisões do layout ao ajustar as lacunas dentro da grade.

.Para nos ajudar a criar LAYOUTS FLEXÍVEIS utilizando o grid, foi criada uma unidade nova. A unidade fr representa uma fração do espaço disponível no container do grid



[Sigla rem]


A rem ("root em") é o tamanho de fonte do elemento-raiz do documento.


#=================================[ Propriedades - GRID CONTEINER ]




#-------------------------[ display


.Propriedade de exibição.

[ grid ou inline-grid]


	. Cria-se um grid container com as declarações CSS display: grid ou display: inline-grid para um elemento da marcação. Assim declarando, todos os elementos filhos diretos daquele container se transformam em grid items.




#-------------------------[ grid-template-colunms(colunas de modelo de grade)

*<https://www.w3schools.com/cssref/pr_grid-template-columns.asp>*
*<https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-columns>*
*<https://medium.com/@mikiasoliveira/como-usar-minmax-no-css-grid-a805520f856a>*


**[Warning - Aqui não estamos definindo apenas A LARGURA das Colunas, mas também a quantidade de colunas existentes nesse grid. ]**

.  define os NOMES DAS LINHAS e as FUNÇÕES DE DIMENSIONAMENTO das trilhas das colunas da grade .


. Define as COLUNAS DE UM CONTÊINER de grade . 

	- Você pode especificar a largura de uma coluna usando uma palavra-chave (como auto) 
	- ou um comprimento (como 10px). 

. O número de colunas É DETERMINADO PELO NÚMERO DE VALORES DEFINIDOS NA LISTA separada por espaços.


	> A propriedade grid-template-columns especifica o número (e as larguras) das colunas em um layout de grade.


	grid-template-columns: none|auto|max-content|min-content|length|initial|inherit;



[ max-content  ]> 

	. Define O TAMANHO de cada coluna, A QUAL VAI DEPENDER DO MAIOR ITEM  da coluna


[ min-content ]> 

	. Define O TAMANHO de cada coluna, A QUAL VAI DEPENDER DO MENOR ITEM da coluna

[ length ]> 

	. Define O TAMANHO DAS COLUNAS, usando um valor de comprimento legal. 

[ initial ]> 

	. Define esta propriedade com seu valor padrão


[ inherit ]

	. Herda essa propriedade de seu elemento pai


[ auto auto auto ]


	. Você pode usar a palavra-chave auto para que cada coluna SE REDIMENSIONE AUTOMATICAMENTE.




[ 40px 1fr 2fr ]


	. Você pode misturar as unidades .


	. Você pode usar a fr unidade flexível para distribuir o espaço restante em todas as colunas flex.


	40px -> Representa o minimo de largura que esse item(ns) Terá


[minmax]


	. A função minmax() torna possível DEFINIR O TAMANHO DE UMA track do grid com um intervalo mínimo e máximo, para que o grid possa se adaptar melhor a viewport de cada usuário da melhor maneira possível.


		.Um grid track é o espaço entre duas linhas em um grid.



		minmax(x, y)

		>minmax(100px, 200px) -> As Colunas  possui o mínimo de 100 e o máximo de 200px
		>minmax(2%,50%) -> As Colunas  possui o mínimo de 2% e o máximo de 50%


		>minmax(1fr, 2fr) -> ESSA REGRA NÃO FAZ SENTIDO, pois o navegador não pode decidir qual valor atribuir à função minmax().


[Auto-fit]

		*utilizado com a função Repeat

	> Define a quantidade de colunas de forma automatica.


	.Preenche todo o contêiner e conforme aumente mais a largura, o tamanho dos itens cresce.


	-  PreenchE O espaço restante do contêiner com colunas e ajusta conforme tamanho necessário ;


[Auto-fill ]

	*utilizado com a função Repeat

	> Define a quantidade de colunas de forma automatica.


	- Preencher o espaço restante do contêiner com colunas vazias
				  e não se preocupa com o tamanho delas;

				  

#-------------------------[ grid-template-rows

	.<https://www.w3schools.com/cssref/pr_grid-template-rows.asp>

**[Warning - Aqui não estamos definindo apenas a ALTURA das linhas, mas também a quantidade de linhas existentes nesse grid. ]**

	>Para criar linhas primeiro deve-se adicionar um número de colunas com a propriedade
	grid-template-columns.

		. Muitas das vezes a quantidade de linhas serão adicionadas/definidas automaticamente conforme o cenário.


. DEFINE AS LINHAS DE UM CONTÊINER de grade . Você pode especificar a ALTURA DE UMA LINHA usando uma palavra-chave:
	
		 (como auto) ou 
		 . um comprimento (como 10px). 


- O número de linhas é determinado pelo NÚMERO DE VALORES definidos na lista separada por espaços.


	grid-template-rows: none|auto|max-content|min-content|length|initial|inherit;

[none]

	. Nenhuma linha está definida.


[Mix de unidades]

	
	grid-template-rows: 120px auto 3rem;



[Com unidade fracionária]


	grid-template-rows: 20px 1fr 2fr;


#-------------------------[ grid-template-areas


	.<https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-areas>
	.<https://www.w3schools.com/cssref/pr_grid-template-areas.asp>

. A propriedade grid-template-areas ESPECIFICA ÁREAS  dentro do layout da grade.



	- A grid-template-areas propriedade CSS especifica áreas de grade nomeadas , estabelecendo as células na grade e atribuindo nomes a elas.



	>A partir dessa propriedade conseguimos nomear células e ao mesmo tempo definir
	linhas e colunas para o grid contêiner






#-------------------------[ Grid-template - Modelo de Grade



. Propriedade ABREVIADA para grid-template-rows, grid-template-columns e grid-template-area


	Sequencia de propriedades:


		. grid-template-rows
		. grid-template-columns
		. grid-template-areas



Sintaxe CSS

grid-template: none|grid-template-rows    / 													   	   |grid-template-columns /
				   |grid-template-areas  /
				   |initial
				   |inherit;





#-------------------------[  Gap (lacuna das Linhas e coluna da grade)

.<https://cssreference.io/css-grid/#grid-gap>
.<https://www.w3schools.com/cssref/pr_grid-column-gap.asp>



[grid-column-gap]
	
	>A propriedade grid-column-gap define o TAMANHO DA LACULA ENTRE AS COLUNAS em um layout de grade.


[grid-row-gap]
	
	>A propriedade grid-row-gap define o TAMANHO DA LACULA ENTRE AS LINHAS em um layout de grade.


[grid-gap]

	>.A propriedade grid-gap define o TAMANHO DA LACULA DAS LINHAS e COLUNAS em um layout de grade.


**[Possíveis Valores]**

[0]

	Remove a lacuna;


[100px]

	pode usar valores em pixel para definir o tamanho da lacuna;


[3rem]

	Pode usar os valores (r) em;


[20%]

	Pode utilizar valores oercentuais;



#-------------------------[ Grid-auto(grade automática)


	. Ao criar linhas e colunas no grid com suas respectivas larguras e auturas,
	pode ocorrer que o grid, crie algumas linhas e colunas de forma automatica,
	as quais não fora pré-defindas em propriedades anteriores, assim temos o grid-auto
	para tratar dessas linhas e colunas de maneira mais eficiente.


		.<https://www.w3schools.com/cssref/pr_grid-auto-columns.asp>
		.<https://cssreference.io/css-grid/#grid-auto-columns>
		.<https://cssreference.io/css-grid/#grid-auto-rows>
		.<https://www.w3schools.com/cssref/pr_grid-auto-rows.asp>
		.<https://www.w3schools.com/cssref/pr_grid-auto-flow.asp>


[grid-auto-columns](Colunas Automatica de Grade)


	> DEFINE O TAMANHO DAS COLUNAS DO GRID que foram criadas de forma IMPLICITA(Que não foi declaradas),OU SEJA, colunas que não foram pre-definidas com alguma propriedades e foram criadas automaticamente pelo grid. Assim tais colunas poderão ser manipuladas com grid-auto-columns.

	>A propriedade grid-auto-columns DEFINE UM TAMANHO para as colunas em um contêiner de grade.

		. Esta propriedade afeta apenas colunas COM O TAMANHO NÃO DEFINIDOS.






	[ Sintaxe CSS ]


		grid-auto-columns: auto | max-content | min-content | length;


[grid-auto-rows](Linhas Automatica de Grade)



	> Define o TAMANHO DAS LINHAS da grade que foram criadas IMPLICITAMENTE: significa que grid-auto-rows direciona as linhas que não foram definidas com grid-template-rows ou grid-template-areas.


	.A grid-auto-rows propriedade DEFINE UM TAMANHO para as linhas em um contêiner de grade.

		- Esta propriedade afeta apenas as linhas com o tamanho não definido.

	[ Sintaxe CSS ]

	
		grid-auto-rows: auto|max-content|min-content|length;


[grid-auto-flow](Fluxo automático de grade)


	>A propriedade grid-auto-flow CONTROLA COMO OS ITENS COLOCADOS AUTOMATICAMENTE(gerado de forma implicita) são inseridos na grade.



	[ Sintaxe CSS ] 

	grid-auto-flow: row|column|dense|row dense|column dense;


	.row:  Valor padrão da linha. Coloca itens preenchendo cada linha

		- Cria novas linhas
	
	.column: Coloca os itens preenchendo cada coluna

		- Cria novas Colunas
	
	.dense: Coloque os itens para preencher todos os buracos na grade

		- Tenta encaixar os itens em espaços que estão sobrando no grid;
	
	.row dense: Coloca os itens preenchendo cada linha e preenche todos os buracos na grade

	.column dense: Coloca os itens preenchendo cada coluna e preenche todos os buracos na grade






#-------------------------[ grid-column-start(início da coluna da grade)


	.<https://www.w3schools.com/cssref/playit.asp?filename=playcss_grid-column-start>
	.<https://cssreference.io/css-grid/#grid-column-start>

. Define a POSIÇÃO INICIAL  da coluna de um item de grade .

	
	- A grid-column-start propriedade define EM QUAL LINHAS DA COLUNA o item começará.

	grid-column-start: NUMBER



	[Sintaxe CSS]

	grid-column-start: auto|span n|column-line;



		auto: Valor padrão. O item será colocado SEGUINDO O FLUXO.

		span n: Especifica o NÚMERO DE COLUNAS que o item irá ABRANGER


		column-line: Especifica em qual coluna




#-------------------------[  grid-column-end(Fim da Coluna de Grade)

	.<https://www.w3schools.com/cssref/pr_grid-column-end.asp>
	.<https://cssreference.io/css-grid/#grid-column-end>

. A grid-column-end propriedade define 

	- QUANTAS COLUNAS UM ITEM VAI ABRANGER, ou 
	- EM QUAL LINHA DE COLUNA o item irá terminar


	. Define a posição final da coluna de um item de grade .

	[ Sintaxe CSS ] 

	grid-column-end: auto|span n|column-line;






#-------------------------[ grid-row-start(Início da Linha de Grade)

	.<https://www.w3schools.com/cssref/pr_grid-row-start.asp>
	.<https://cssreference.io/css-grid/#grid-row-start>
	.<https://www.w3schools.com/cssref/playit.asp?filename=playcss_grid-row-start>


. Define a POSIÇÃO INICIAL da linha de um item de grade .
	


. A grid-row-start propriedade define em qual linha o item começará.





	[ Sintaxe CSS ]

 	grid-row-start: auto|row-line;



 	Auto: Valor padrão. O item será colocado seguindo o fluxo.

 	row-line: Especifica em qual linha iniciar a exibição do item.






#-------------------------[ grid-row-end (Final da Linha de Grade)


	.<https://www.w3schools.com/cssref/pr_grid-row-end.asp>
	.<https://www.w3schools.com/cssref/playit.asp?filename=playcss_grid-row-end>
	.<https://cssreference.io/css-grid/#grid-row-start>

	
.A grid-row-end propriedade define 

	- QUANTAS LINHAS UM ITEM IRÁ ABRANGER, ou 
	- EM QUAL LINHA O ITEM TERMINARÁ



#-------------------------[ Justify-content(justifique-conteúdo)


. Justifica(Alinha) os ITENS DO GRID em relação ao eixo x(Horizontal)

**[ Justificando a célula inteira do grid ]**

	- A justify-content propriedade ALINHA OS ITENS do container flexível 
	QUANDO OS ITENS NÃO UTILIZAM TODO O ESPAÇO DISPONÍVEL no eixo principal (horizontal).

>Para mostrar como a Propriedade Funciona, precisamos ter um Espaço disponível 
no Grid;


[ Sintaxe CSS ]

justify-content: 

	- start : Justifica os itens ao início.

	- end:  Justifica os itens ao final.

	- center:  Centraliza o conteúdo.

	- space-between: CRIA um espaço entre os elementos, ignorando o início e final;
	
	- space-around: Distribui um espaço entre os elementos.(O início e final são MENORES que os espaços internos);

	- space-evenly: Cria um espaço igual Entre AS COLUNAS(No início e final também)

	- stretch: Estica os itens.

	- inherit;



#-------------------------[ Align-content(Alinhamento de Conteúdo)

	- Alinha os itens do grid em RELAÇÃO AO EIXO y(vertical)


**[ Justificando a célula inteira do grid ]**

. A propriedade align-content modifica o comportamento da propriedade flex-wrap . É semelhante a align-items , mas em vez de alinhar itens flexíveis, ele ALINHA LINHAS FLEXÍVEIS.

Sintaxe CSS
align-content: 

	
	- center: Centraliza o conteúdo;
	
	- start: Alinha os Itens ao Início do Eixo Vertical;
	
	- end: Alinha os Itens no Final do Eixo Vertical;
	
	- space-between: Cria um espaço entre os Itens.(Ignorando o Início e o final)
	
	- space-around: Distribui o espaço entre os Itens.(O espaço do início e do final são menores);
	
	- espace-evenly: Distribui um espaço igual entre os Itens.(No Início e no Final também)

	- stretch: Estica os itens;



#-------------------------[ Justify-items(jUSTIFICAR ITENS)

. Justifica o conteúdo dos itens do Grid em relação ao Eixo x(Horizontal).


**[ Justificando em relação a célula dentro do grid]**;

	

	- A propriedade justify-content ALINHA os itens do container flexível 
	QUANDO OS ITENS NÃO UTILIZAM TODO O ESPAÇO(Dentro da célula/grade em que está) disponível no eixo principal (horizontal).

	justify-content: 

		- flex-start
		- flex-end
		- center


#-------------------------[ Align-items

**[ Justificando em relação a célula dentro do grid]**;

	
. Alinha o conteúdo dos items do grid em RELAÇÃO AO EIXO Y(Vertical);

	- Alinha em relação a Célula
	

	align-items: 

		- stretch
		- center
		- start
		- end
		- baseline
		- initial
		- inherit;



#=================================[ Propriedades - GRID ITEM ]






#-------------------[ grid-column ]


. A propriedade abreviada grid-column CSS especifica o tamanho e a localização de um item de grade dentro de uma coluna de grade. 

.A propriedade grid-column especifica o TAMANHO e a LOCALIZAÇÃO de UM item de grade em um layout de grade e é uma propriedade abreviada para as seguintes propriedades:


	- grid-column-start
	- grid-column-end


	Sintaxe CSS

	grid-column: grid-column-start / grid-column-end;

	

#-------------------[ grid-row ]

	.<https://developer.mozilla.org/en-US/docs/Web/CSS/grid-row>

. A propriedade abreviada grid-row de CSS especifica o TAMANHO e a LOCALIZAÇÃO de um item de grade dentro da linha da grade, contribuindo com:

		- uma linha, 
		- um intervalo ou 
		- nada (automático) para seu posicionamento de grade


Esta propriedade é um atalho para as seguintes propriedades CSS:

	grid-row-end
	grid-row-start




#-------------------[ grid-area ]

<https://www.w3schools.com/cssref/pr_grid-area.asp>
<https://developer.mozilla.org/en-US/docs/Web/CSS/grid-area>


. A  propriedade grid-area especifica o tamanho e a localização de um item de grade em um layout de grade e é uma propriedade abreviada para as seguintes propriedades:

	- grid-row-start
	- grid-coluna-início
	- grid-row-end
	- final da coluna da grade

A propriedade grid-area também pode ser usada para atribuir um nome a um item de grade. 

[Z-index]

	Pode ser utilizado para MANIPULAR a posição no EIXO Z do item. Ou seja, se
	um item for posicionado em CIMA de outro, o z-indez controla qual vêm na frente;



	grid-area: 1 / 2 / 4 / 3

	Grid-row-start: 1;
	grid-column-start: 2;
	grid-row-end: 4;
	grid-column-end: 3;



#-------------------[ justify-self ]

#-------------------[ align-self ]





















/*===================================================================================*/




#---------------[ nth-child


	. seleciona elementos filhos com base em suas posições dentro do bloco Pai/Principal.

	n - pode ser um número, uma palavra-chave ou uma fórmula.



		Ex: 3n + 1

			1 -> Número inicial/elemento que será aplciado a estilização;
			3n -> E depois do 1, selecionar sempre o terceiro elemento/item;





